"use strict";
var books = require('./books');
/*
 * Arrow functions
 * An arrow function expression has a shorter syntax compared to function expressions and lexically binds
 * the value of the this operator.
 */
var Person2 = (function () {
    function Person2(name) {
        this.name = name;
    }
    Person2.prototype.greet = function () {
        alert("Hi! My name is " + this.name); // this operator points to the object that encloses the greet method.
    };
    /*
     * As soon as we define an anonymous function (the callback), the this keyword changes its value and starts
     * pointing to the anonymous function.
     */
    Person2.prototype.greetDelay = function (time) {
        setTimeout(function () {
            alert("Hi! My name is " + this.name);
        }, time);
    };
    /*
     * arrow functions allows us to add a function without altering the value of this operator.
     * By using an arrow function, we can ensure that the this operator still points to the Person
     * instance and not to the setTimeout callback.
     */
    Person2.prototype.greetDelay2 = function (time) {
        var _this = this;
        setTimeout(function () {
            alert("Hi! My name is " + _this.name);
        }, time);
    };
    return Person2;
}());
// var remo = new Person2("Remo");
// remo.greet(); // "Hi! My name is Remo
// remo.greetDelay(1000); // "Hi! My name is
// remo.greetDelay2(1000); // "Hi! My name is Remo
function TestArrowFunctions() {
    var allBooks = books.GetAllBooks();
    books.LogFirstAvailable(allBooks);
    var arr = allBooks.filter(function (book) {
        return book.author == 'Herman Melville';
    });
    // Same function written in arrow syntax (with type annotations)
    arr = allBooks.filter(function (book) { return book.author == 'Herman Melville'; });
    // Same function written in arrow syntax (short way)
    arr = allBooks.filter(function (book) { return book.author == 'Herman Melville'; });
    // no params: parenthesis required.
    allBooks.forEach(function () { return console.log('done reading!'); });
    // 1 param: parenthesis are optional.
    allBooks.forEach(function (title) { return console.log(title); });
    // multiple params: parenthesis required.
    allBooks.forEach(function (book, idx, arr) { return console.log(idx + " - " + book.title); });
    /*
    * Note:
    * In arrow functions, everything on the left is a parameter, and everything on the right is
    * the function body. if the function does not accept a parameter, or if there's more than
    * one parameter, you must add a set of parenthesis.
    *
    * Arrow functions lexically binds the value of the this keyword. In other
    * words, the keyword is captured at function creation, not invocation.
    *
    * Side note:
    * the 'this' keyword is set at the time a function is executed.
    */
}
exports.TestArrowFunctions = TestArrowFunctions;
