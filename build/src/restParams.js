"use strict";
var books = require('./books');
function CheckoutBooks(customer) {
    var bookIds = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        bookIds[_i - 1] = arguments[_i];
    }
    console.log("Checking out books for " + customer);
    var booksCheckedOut = [];
    bookIds.forEach(function (id) {
        var book = books.GetBookById(id);
        if (book != null && book.available)
            booksCheckedOut.push(book.title);
    });
    return booksCheckedOut;
}
exports.CheckoutBooks = CheckoutBooks;
