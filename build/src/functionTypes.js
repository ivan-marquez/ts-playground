"use strict";
var books = require('./books');
function PublicationMessage(year) {
    return "Date published: " + year;
}
exports.PublicationMessage = PublicationMessage;
function CreateCustomerId(name, id) {
    return name + " " + id;
}
exports.CreateCustomerId = CreateCustomerId;
function GetBookById(id) {
    return books.GetAllBooks().filter(function (book) { return book.id === id; });
}
exports.GetBookById = GetBookById;
//let IdGenerator: (chars: string, nums: number) => string;
var IdGenerator = function (name, id) { return id + " " + name; };
exports.IdGenerator = IdGenerator;
