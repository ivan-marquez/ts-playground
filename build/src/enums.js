"use strict";
var Category;
(function (Category) {
    Category[Category["Science"] = 5] = "Science";
    Category[Category["Physics"] = 7] = "Physics";
    Category[Category["Research"] = 9] = "Research";
})(Category || (Category = {}));
exports.Category = Category;
