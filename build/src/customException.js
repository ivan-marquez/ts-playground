var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var customException;
(function (customException) {
    var Exception = (function (_super) {
        __extends(Exception, _super);
        function Exception(message) {
            _super.call(this, message);
            this.message = message;
            this.name = 'Exception';
            this.message = message;
            this.stack = (new Error()).stack;
        }
        Exception.prototype.toString = function () {
            return this.name + ': ' + this.message;
        };
        return Exception;
    }(Error));
    customException.Exception = Exception;
})(customException || (customException = {}));
/*
 * In the preceding code snippet, we have declared a class named Error. This class is available at
 * runtime but is not declared by TypeScript, so we will have to do it ourselves. Then, we have
 * created an Exception class, which inherits from the Error class.
 */
