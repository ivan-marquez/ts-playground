"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Q = require('q');
var $ = require('jquery');
/*
 * The Email class allows us to use emails without having to worry about e-mail validation
 * because the class will deal with it for us. We can make this clearer by using access
 * modifiers (public or private) to flag as private all the class attributes and methods
 * that we want to abstract from the use of the Email class:
 */
var Email = (function () {
    function Email(email) {
        if (this.validateEmail(email))
            this._email = email;
        else
            throw new Error("Invalid email!");
    }
    Email.prototype.validateEmail = function (email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    };
    Email.prototype.get = function () {
        return this._email;
    };
    return Email;
}());
exports.Email = Email;
/*
 * Single Responsibility Principle:
 * Making sure that a class has a single responsibility makes it easier to see what it does
 * and how we can extend/improve it. We can further improve our Person and Email classes by
 * increasing the level of abstraction of our classes. For example, when we use the Email class,
 * we don't really need to be aware of the existence of the validateEmail method; so this method
 * could be invisible from outside the Email class. As a result, the Email class would be much
 * simpler to understand.
 */
var Person = (function () {
    function Person(name, surname, email) {
        this.email = email;
        this.name = name;
        this.surname = surname;
    }
    Person.prototype.greet = function () {
        console.log("Hi!");
    };
    return Person;
}());
// Inheritance
var Teacher = (function (_super) {
    __extends(Teacher, _super);
    function Teacher(name, surname, email, subjects) {
        _super.call(this, name, surname, email); //parent class constructor
        this._subjects = subjects;
    }
    Teacher.prototype.teach = function () {
        console.log('Welcome to class!');
    };
    Teacher.prototype.greet = function () {
        _super.prototype.greet.call(this);
        console.log('I teach' + this._subjects);
    };
    Teacher.prototype.get = function () {
        return this._subjects;
    };
    Teacher.prototype.Dev = function () {
    };
    return Teacher;
}(Person));
var SchoolPrincipal = (function (_super) {
    __extends(SchoolPrincipal, _super);
    function SchoolPrincipal() {
        _super.apply(this, arguments);
    }
    SchoolPrincipal.prototype.manageTeachers = function () {
        console.log('We need to help students to get better results!');
    };
    return SchoolPrincipal;
}(Teacher));
var principal = new SchoolPrincipal('remo', 'jansen', new Email('remo@dev.com'), ['math', 'science']);
principal.greet();
principal.teach();
principal.manageTeachers();
//Mixins
/*
 * We need to copy the following function somewhere in our code to be able to apply mixins.
 * This function is a well-known pattern and can be found in many books and online
 * references, including the official TypeScript handbook.
 */
function applyMixins(derivedCtor, baseCtors) {
    baseCtors.forEach(function (baseCtor) {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(function (name) {
            if (name !== 'constructor') {
                derivedCtor.prototype[name] = baseCtor.prototype[name];
            }
        });
    });
}
var Animal = (function () {
    function Animal() {
    }
    Animal.prototype.eat = function () {
        return "Delicious!";
    };
    return Animal;
}());
var Mammal = (function (_super) {
    __extends(Mammal, _super);
    function Mammal() {
        _super.apply(this, arguments);
    }
    Mammal.prototype.breathe = function () {
        return "I'm alive!";
    };
    Mammal.prototype.move = function () {
        return "I can move like a mammal!";
    };
    return Mammal;
}(Animal));
var WingedAnimal = (function (_super) {
    __extends(WingedAnimal, _super);
    function WingedAnimal() {
        _super.apply(this, arguments);
    }
    WingedAnimal.prototype.fly = function () {
        return "I can fly!";
    };
    WingedAnimal.prototype.move = function () {
        return "I can move like a bird!";
    };
    return WingedAnimal;
}(Animal));
var Bat = (function () {
    function Bat() {
    }
    return Bat;
}());
/*
 * We have used the reserved keyword implements (as opposed to extends) to indicate that
 * Bat will implement the functionality declared in both the Mammal and WingedAnimal classes.
 * We have also added the signature of each of the methods that the Bat class will implement.
 */
applyMixins(Bat, [WingedAnimal, Mammal]);
/*
 * Mixins have some limitations. The first limitation is that we can only inherit the properties
 * and methods FROM ONE LEVEL IN THE INHERITANCE TREE. The second limitation is that, if two or
 * more of the parent classes contain a method with the same name, THE METHOD THAT IS GOING TO BE
 * INHERITED WILL BE TAKEN FROM THE LAST CLASS PASSED IN THE BASECTORS ARRAY TO THE APPLYMIXINS
 * FUNCTION.
 */
var bat = new Bat();
//bat.eat(); //Error: not avaliable.
bat.breathe();
bat.fly();
bat.move(); //I can move like a mammal!
var DevUser = (function () {
    function DevUser() {
    }
    DevUser.prototype.isValid = function () {
        //User validation logic
        return true;
    };
    return DevUser;
}());
var Talk = (function () {
    function Talk() {
    }
    Talk.prototype.isValid = function () {
        //Talk validation logic
        return true;
    };
    return Talk;
}());
/*
 * Open / Close Principle:
 * If we hide the validation logic through the IValidatable interface (open for extension),
 * no additional changes will be required for the Repository class (close for modification).
 */
var Repository = (function () {
    function Repository(url) {
        this._url = url;
    }
    Repository.prototype.getAsync = function () {
        var _this = this;
        return Q.Promise(function (resolve, reject) {
            $.ajax({
                url: _this._url,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    var list;
                    var items = data.items;
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].isValid()) {
                            list.push(items[i]);
                        }
                    }
                    resolve(list);
                },
                error: function (e) {
                    reject(e);
                }
            });
        });
    };
    return Repository;
}());
/*
 * Even though we have used an interface, we used the extends keyword and not the
 * implements keyword to declare the constraint in the preceding example. THERE IS
 * NO SPECIAL REASON FOR THAT. THIS IS JUST THE WAY THE TYPESCRIPT CONSTRAINT
 * SYNTAX WORKS.
 *
 * NOTE:
 * We cannot specify multiple types when declaring a generic type constraint. However, we can
 * work around this issue by transforming the required interfaces in super-interfaces and having
 * a child interface that extends this super-interfaces.
 */
var userRepository = new Repository("/api/users");
userRepository.getAsync().then(function (users) {
    console.log('userRepository => ', users);
});
var talkRepository = new Repository("/api/talk");
talkRepository.getAsync().then(function (talks) {
    console.log('talkRepository => ', talks);
});
// new operator in generic type constraints
function factory() {
    var type;
    return new type;
}
var CookiePersitanceService = (function () {
    function CookiePersitanceService() {
    }
    CookiePersitanceService.prototype.save = function (entity) {
        var id = Math.floor((Math.random() * 100) + 1);
        // Cookie persistance logic...
        return id;
    };
    return CookiePersitanceService;
}());
var LocalStoragePersitanceService = (function () {
    function LocalStoragePersitanceService() {
    }
    LocalStoragePersitanceService.prototype.save = function (entity) {
        var id = Math.floor((Math.random() * 100) + 1);
        // Local storage persistance logic...
        return id;
    };
    return LocalStoragePersitanceService;
}());
var FavouritesController = (function () {
    function FavouritesController(persistanceService) {
        this._persistanceService = persistanceService;
    }
    FavouritesController.prototype.saveAsFavorite = function (articleId) {
        return this._persistanceService.save(articleId);
    };
    return FavouritesController;
}());
/*
 * The LSP allows us to replace a dependency with another implementation as long as
 * both implementations are based in the same base type; so, if we decide to stop using
 * cookies as storage and use the HTML5 local storage API instead, we can do it freely
 * without having to add any changes to the FavouritesController class.
 */
//var favController = new FavouritesController(new CookiePersitanceService());
var favController = new FavouritesController(new LocalStoragePersitanceService());
// Class Expressions
var devExpression = (function (_super) {
    __extends(class_1, _super);
    function class_1() {
        _super.apply(this, arguments);
    }
    class_1.prototype.Dev = function () {
        console.log('implementing abstract method.');
    };
    return class_1;
}(Person));
exports.devExpression = devExpression;
