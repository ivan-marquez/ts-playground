"use strict";
var enums_1 = require('./enums');
function GetAllBooks() {
    var books = [{
            id: 1,
            title: 'Ulisses',
            author: 'Jams Joyce',
            available: true,
            category: enums_1.Category.Physics,
            markDamaged: function (reason) { return console.log("Damaged: " + reason); }
        }, {
            id: 2,
            title: 'A farewell to Arms',
            author: 'Ernest Hemingway',
            available: false,
            category: enums_1.Category.Research
        }, {
            id: 3,
            title: 'I Know Why the Caged bird Sings',
            author: 'Maya Angalou',
            available: true,
            category: enums_1.Category.Science
        }];
    return books;
}
exports.GetAllBooks = GetAllBooks;
function LogFirstAvailable(books) {
    var numberOfBooks = books.length;
    var firstAvailable;
    for (var _i = 0, books_1 = books; _i < books_1.length; _i++) {
        var currentBook = books_1[_i];
        if (currentBook.available) {
            firstAvailable = currentBook.title;
            break;
        }
    }
    console.log("Total Books: " + numberOfBooks);
    console.log("First Available: " + firstAvailable);
}
exports.LogFirstAvailable = LogFirstAvailable;
function GetBookById(id) {
    var allBooks = GetAllBooks();
    return allBooks.filter(function (book) { return book.id === id; })[0]; //.First() in c#
}
exports.GetBookById = GetBookById;
function PrintBook(book) {
    console.log(book.title + " by " + book.author);
}
exports.PrintBook = PrintBook;
