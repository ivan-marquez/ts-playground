/**
 * Created by Ivan on 4/18/16.
 */
var Logger = (function () {
    function Logger() {
    }
    Logger.prototype.log = function (arg) {
        if (typeof console.log === "function")
            console.log(arg);
        else
            alert(arg);
    };
    return Logger;
}());
