/// <reference path="../typings/globals/jquery/index.d.ts" />
/// <reference path="../typings/globals/handlebars/index.d.ts" />
/// <reference path="../typings/globals/Q/index.d.ts" />
//Function Types
var greetUnnamed;
greetUnnamed = function (name) {
    if (name)
        return 'Hi' + name;
};
//Declaration and Assignment
var greetUnnamed2 = function (name) {
    if (name)
        return 'Hi' + name;
};
//Optional Parameters
function add(foo, bar, foobar) {
    foo += bar;
    if (foobar !== undefined)
        foo += foobar;
    return foo;
}
//Default Parameters
function add2(foo, bar, foobar) {
    if (foobar === void 0) { foobar = 0; }
    return foo + bar + foobar;
}
//Rest Parameters
function restAdd() {
    var foo = [];
    for (var _a = 0; _a < arguments.length; _a++) {
        foo[_a - 0] = arguments[_a];
    }
    var result = 0;
    for (var i = 0; i < foo.length; i++) {
        result += foo[i];
    }
    return result;
}
/*
 * add(); // returns 0
 * add(2); // returns 2
 * add(2,2,2); // returns 6
 */
//Use an array to avoid additional iteration
function add4(foo) {
    var result = 0;
    for (var i = 0; i < foo.length; i++) {
        result += foo[i];
    }
    return result;
}
//All signatures must be compatible (same return type).
//Implementation signature - always last in the list.
function overload(value) {
    switch (typeof value) {
        case "string":
            return "My name is " + value + ".";
        case "number":
            return "I'm " + value + " years old.";
        case "boolean":
            return value ? "I'm single." : "I'm not single.";
        default:
            console.log("Invalid Operation!");
    }
}
//When we declare a specialized signature in an object, it must be
//assignable to at least one non-specialized signature in the same object.
//Function Scope
function foo() {
    if (true) {
        var bar = 0;
    }
    alert(bar);
}
//foo(); shows 0
//If we invoke the foo function, the alert function will be able to display the variable
//bar without errors because all the variables inside a function will be in the scope of
//the entire function body, even if they are inside another block of code (except a
//function block). At runtime, all the variable declarations are moved to the top of a
//function before the function is executed. This behavior is called hoisting.
//Using let - it allows us to set the scope of a variable to a block (if, while, for...)
//rather than a function block.
function foo2() {
    if (true) {
        var bar = 0;
        bar = 1;
    }
    //alert(bar); error!
}
//While variables defined with const follow the same scope rules as variables declared with let,
//they can't be reassigned:
function foo3() {
    if (true) {
        var bar = 0;
    }
    //alert(bar); error!
}
//Classes (IIFE's)
var Counter = (function () {
    function Counter() {
        this._i = 0;
    }
    Counter.prototype.get = function () {
        return this._i;
    };
    Counter.prototype.set = function (val) {
        this._i = val;
    };
    Counter.prototype.increment = function () {
        this._i++;
    };
    return Counter;
}());
var counter = new Counter();
console.log(counter.get()); // 0
counter.set(2);
console.log(counter.get()); // 2
counter.increment();
console.log(counter.get()); // 3
//console.log(counter._i); // Error: Property '_i' is private
//Generics
var User = (function () {
    function User() {
    }
    return User;
}());
var Order = (function () {
    function Order() {
    }
    return Order;
}());
function getUsers(cb) {
    $.ajax({
        url: 'api/users',
        method: 'GET',
        success: function (data) {
            cb(data.items);
        },
        error: function (error) {
            cb(null);
        }
    });
}
getUsers(function (users) {
    for (var i = 0; i < users.length; i++) {
        console.log(users[i].name);
    }
});
//Generic function
function getEntities(url, cb) {
    $.ajax({
        url: 'api/users',
        method: 'GET',
        success: function (data) {
            cb(data.items);
        },
        error: function (error) {
            cb(null);
        }
    });
}
//Usage
getEntities('/api/users', function (users) {
    for (var i = void 0; users.length; i++) {
        console.log(users[i].name);
    }
});
getEntities('/api/orders', function (orders) {
    for (var i = void 0; orders.length; i++) {
        console.log(orders[i].total);
    }
});
function Purge(inventory) {
    return inventory.splice(2, inventory.length);
}
//Tag functions
function htmlEscape(literals) {
    var placeholders = [];
    for (var _a = 1; _a < arguments.length; _a++) {
        placeholders[_a - 1] = arguments[_a];
    }
    var result = "";
    for (var i = 0; i < placeholders.length; i++) {
        result += literals[i];
        result += placeholders[i]
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }
    result += literals[literals.length - 1];
    return result;
}
var html = (_a = ["<h1>", "</h1>"], _a.raw = ["<h1>", "</h1>"], htmlEscape(_a, name));
// The preceding function iterates through the literals and values and ensures
// that the HTML code is escaped from the values to avoid possible code injection attacks.
// The main benefit of using a tagged function is that it allows us to create custom
// template string processors.
//Asynchronous programming
var foo4 = function () {
    console.log('foo');
};
function bar4(cb) {
    console.log('bar');
    cb();
}
bar4(foo4); // prints 'bar' then prints 'foo'.
var View = (function () {
    function View(config) {
        this._appendHtml = function (html, cb, errorCb) {
            try {
                if ($(this._container).length === 0) {
                    throw new Error("Container not found!");
                }
                $(this._container).html(html);
                cb($(this._container));
            }
            catch (e) {
                errorCb(e);
            }
        };
        this._container = config.container;
        this._templateUrl = config.templateUrl;
        this._serviceUrl = config.serviceUrl;
        this._args = config.args;
    }
    View.prototype._loadJson = function (url, args, cb, errorCb) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: args,
            success: function (json) {
                cb(json);
            },
            error: function (json) {
                errorCb(json);
            }
        });
    };
    View.prototype._loadHbs = function (url, cb, errorCb) {
        $.ajax({
            url: url,
            type: "GET",
            dataType: "text",
            success: function (hbs) {
                cb(hbs);
            },
            error: function (e) {
                errorCb(e);
            }
        });
    };
    View.prototype._compileHbs = function (hbs, cb, errorCb) {
        try {
            var template = Handlebars.compile(hbs);
            cb(template);
        }
        catch (e) {
            errorCb(e);
        }
    };
    View.prototype._jsonToHtml = function (template, json, cb, errorCb) {
        try {
            var html = template(json);
            cb(html);
        }
        catch (e) {
            errorCb(e);
        }
    };
    View.prototype.render = function (cb, errorCb) {
        var _this = this;
        try {
            this._loadJson(this._serviceUrl, this._args, function (json) {
                _this._loadHbs(_this._templateUrl, function (hbs) {
                    _this._compileHbs(hbs, function (template) {
                        _this._jsonToHtml(template, json, function (html) {
                            _this._appendHtml(html, cb, errorCb);
                        }, errorCb);
                    }, errorCb);
                }, errorCb);
            }, errorCb);
        }
        catch (e) {
            errorCb(e);
        }
    };
    return View;
}());
var ViewAsync = (function () {
    function ViewAsync(config) {
        this._container = config.container;
        this._templateUrl = config.templateUrl;
        this._serviceUrl = config.serviceUrl;
        this._args = config.args;
    }
    ViewAsync.prototype._loadJsonAsync = function (url, args) {
        return Q.Promise(function (resolve, reject) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                data: args,
                success: function (json) {
                    resolve(json);
                },
                error: function (e) {
                    reject(e);
                }
            });
        });
    };
    ViewAsync.prototype._loadHbsAsync = function (url) {
        return Q.Promise(function (resolve, reject) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "text",
                success: function (hbs) {
                    resolve(hbs);
                },
                error: function (e) {
                    reject(e);
                }
            });
        });
    };
    ViewAsync.prototype._compileHbsAsync = function (hbs) {
        return Q.Promise(function (resolve, reject) {
            try {
                var template = Handlebars.compile(hbs);
                resolve(template);
            }
            catch (e) {
                reject(e);
            }
        });
    };
    ViewAsync.prototype._jsonToHtmlAsync = function (template, json) {
        return Q.Promise(function (resolve, reject) {
            try {
                var html = template(json);
                resolve(html);
            }
            catch (e) {
                reject(e);
            }
        });
    };
    ViewAsync.prototype._appendHtmlAsync = function (html, container) {
        return Q.Promise(function (resolve, reject) {
            try {
                var $container = $(container);
                if ($container.length === 0) {
                    throw new Error("Container not found!");
                }
                $container.html(html);
                resolve($container);
            }
            catch (e) {
                reject(e);
            }
        });
    };
    ViewAsync.prototype.renderAsync = function () {
        var _this = this;
        return Q.Promise(function (resolve, reject) {
            try {
                // assign promise to getJson - Series
                var getJson = _this._loadJsonAsync(_this._serviceUrl, _this._args);
                // assign promise to getTemplate - Waterfall
                var getTemplate = _this._loadHbsAsync(_this._templateUrl)
                    .then(_this._compileHbsAsync);
                // execute promises in parallel - Concurrent
                Q.all([getJson, getTemplate]).then(function (results) {
                    var json = results[0];
                    var template = results[1];
                    _this._jsonToHtmlAsync(template, json)
                        .then(function (html) {
                        return _this._appendHtmlAsync(html, _this._container);
                    })
                        .then(function ($container) {
                        resolve($container);
                    });
                });
            }
            catch (error) {
                reject(error);
            }
        });
    };
    return ViewAsync;
}());
var _a;
