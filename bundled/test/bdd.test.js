(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.test = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
///<reference path="./interfaces.ts" />
/// <reference path="../typings/globals/jquery/index.d.ts" />
"use strict";
var CalculatorWidget = (function () {
    function CalculatorWidget(math) {
        if (math == null)
            throw new Error("Argument null exception!");
        this._math = math;
    }
    CalculatorWidget.prototype.render = function (id) {
        var _this = this;
        $(id).html(template);
        this.$base = $("#base");
        this.$exponent = $("#exponent");
        this.$result = $("#result");
        this.$btn = $("#submit");
        this.$btn.on("click", function (e) {
            _this.onSubmit();
        });
    };
    CalculatorWidget.prototype.onSubmit = function () {
        var base = parseInt(this.$base.val());
        var exponent = parseInt(this.$exponent.val());
        if (isNaN(base) || isNaN(exponent)) {
            alert("Base and exponent must be a number!");
        }
        else {
            this.$result.val(this._math.pow(base, exponent));
        }
    };
    return CalculatorWidget;
}());
exports.CalculatorWidget = CalculatorWidget;
// normally we will use a template system
var template = '<div class="well">' +
    '<div class="row">' +
    '<div class="col-md-3">' +
    '<div class="form-group">' +
    '<label>Base</label>' +
    '<input type="text" class="form-control" id="base" placeholder="0">' +
    '</div>' +
    '</div>' +
    '<div class="col-md-3">' +
    '<div class="form-group">' +
    '<label>Exponent</label>' +
    '<input type="text" class="form-control" id="exponent" placeholder="0">' +
    '</div>' +
    '</div>' +
    '<div class="col-md-3">' +
    '<div class="form-group">' +
    '<label>Result</label>' +
    '<input type="text" class="form-control" id="result" placeholder="1" disabled="disabled">' +
    '</div>' +
    '</div>' +
    '<div class="col-md-3">' +
    '<div class="form-group">' +
    '<button id="submit" type="submit" class="btn btn-primary">Submit</button>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';

},{}],2:[function(require,module,exports){
///<reference path="./interfaces.ts" />
"use strict";
var MathDemo = (function () {
    function MathDemo() {
        this.PI = 3.14159265359;
    }
    // used to showcase how to test a sync function
    MathDemo.prototype.pow = function (base, exponent) {
        var result = base;
        for (var i = 1; i < exponent; i++) {
            result = result * base;
        }
        return result;
    };
    // used to show case how to test async code with done()
    MathDemo.prototype.powAsync = function (base, exponent, cb) {
        var result = this.pow(base, exponent);
        cb(result);
    };
    // simulate slow > 40ms
    MathDemo.prototype.powAsyncSlow = function (base, exponent, cb) {
        var _this = this;
        setTimeout(function () {
            var result = _this.pow(base, exponent);
            cb(result);
        }, 45);
    };
    // simulate reelly slow function > 100ms
    MathDemo.prototype.powAsyncReallySlow = function (base, exponent, cb) {
        var _this = this;
        var result = base ^ exponent;
        setTimeout(function () {
            var result = _this.pow(base, exponent);
            cb(result);
        }, 101);
    };
    // simulate too slow > 2000ms (breaks build)
    MathDemo.prototype.powAsyncTooSlow = function (base, exponent, cb) {
        var _this = this;
        var result = base ^ exponent;
        setTimeout(function () {
            var result = _this.pow(base, exponent);
            cb(result);
        }, 2001);
    };
    // used to showcase how to assert that an error is thrown
    MathDemo.prototype.bad = function (foo) {
        if (foo == null) {
            throw new Error("Error!");
        }
        else {
        }
    };
    return MathDemo;
}());
exports.MathDemo = MathDemo;

},{}],3:[function(require,module,exports){
///<reference path="../src/interfaces.ts" />
/// <reference path="../typings/index.d.ts" />
"use strict";
var math_demo_1 = require("../src/math_demo");
var calculator_widget_1 = require("../src/calculator_widget");
// Here we will write some tests for the demos in the source
// directory using a BDD style. BDD style assertions are
// provided by expect() and should() which use a chainable
// language to construct assertions. The should()
// function has some issues when used in Internet Explorer,
// so it will not be used in this demo.
var expect = chai.expect; // http://chaijs.com/guide/styles/#expect
// describe() is used to declare a test suite
describe('BDD test example for MathDemo class \n', function () {
    // before() is invoked once before ALL tests
    before(function () {
        console.log("before() invoked!");
    });
    // after() invoked once after ALL tests
    after(function () {
        console.log("after() invoked!");
    });
    // beforeEach() is invoked once before EACH test
    beforeEach(function () {
        console.log("beforeEach() invoked!");
    });
    // afterEach() is invoked once before EACH test
    afterEach(function () {
        console.log("afterEach() invoked!");
    });
    // it() is a single test containing one or more assertions
    it('should return the correct numeric value for PI \n', function () {
        var math = new math_demo_1.MathDemo();
        expect(math.PI).to.equals(3.14159265359);
        expect(math.PI).to.be.a('number');
    });
    it('should return the correct numeric value for pow \n', function () {
        var math = new math_demo_1.MathDemo();
        var result = math.pow(2, 3);
        var expected = 8;
        expect(result).to.be.a('number');
        expect(result).to.equal(expected);
    });
    // to test async code we need to invoke done() when the execution is completed
    it('should return the correct numeric value for pow (async) \n', function (done) {
        var math = new math_demo_1.MathDemo();
        math.powAsync(2, 3, function (result) {
            var expected = 8;
            expect(result).to.be.a('number');
            expect(result).to.equal(expected);
            done(); // invoke done() inside your call back or fulfilled promises
        });
    });
    /*
    // When testing async code mocha will let us know if a function takes too long
    // to finish its execution. There are 3 levels of warning
    // 1. >   40ms low warning
    // 2. >  100ms warning
    // 3. > 2000ms fatal error (execution of test will not continue)
  
    // this function takes over 2000ms to complete and
    // will thereofore stop the test execution
  
    it('too slow will cause build to fail\n', (done) => {
      var math : MathInterface = new MathDemo();
      math.powAsyncTooSlow(2, 3, function(result){
        var expected = 8;
        expect(result).to.be.a('number');
        expect(result).to.equal(expected);
        done(); // invoke done() inside your call back or fulfilled promises
      });
    });
    */
    // how to test for errors
    it('should throw an exception when no parameters passed \n', function () {
        var math = new math_demo_1.MathDemo();
        expect(math.bad).to.throw(Error);
    });
});
describe('BDD test example for CalculatorWidget class \n', function () {
    before(function () {
        $("body").append('<div id="widget"/>');
    });
    beforeEach(function () {
        $("#widget").empty();
    });
    // showcases how to spy on functions to assert that a function has been invoked
    it('should invoke onSubmit when #submit.click is triggered \n', function () {
        var math = new math_demo_1.MathDemo();
        var calculator = new calculator_widget_1.CalculatorWidget(math);
        calculator.render("#widget");
        // spy on onSubmit
        var onSubmitSpy = sinon.spy(calculator, "onSubmit");
        // initialize inputs and trigger click on #submit
        $('#base').val("2");
        $('#exponent').val("3");
        $("#submit").trigger("click");
        // assert calculator.onSubmit was invoked
        expect(onSubmitSpy.called).to.equal(true);
        expect(onSubmitSpy.callCount).to.equal(1);
        expect($("#result").val()).to.equal('8');
    });
    // showcases how to use stub to isolate a component being
    // tested (CalculatorWidget) from its dependencies (MathDemo)
    // also showcases how to test async code
    it('onSubmit should set #result value when #submit.click is triggered \n', function (done) {
        var math = new math_demo_1.MathDemo();
        // replace pow method with stub
        sinon.stub(math, "pow", function (a, b) {
            // assert that CalculatorWidget.onSubmit is invoking
            // math.pow with the rigth arguments
            expect(a).to.equal(2);
            expect(b).to.equal(3);
            done();
        });
        // initialize inputs and trigger click on #submit
        var calculator = new calculator_widget_1.CalculatorWidget(math);
        calculator.render("#widget");
        $('#base').val("2");
        $('#exponent').val("3");
        $("#submit").trigger("click");
    });
});

},{"../src/calculator_widget":1,"../src/math_demo":2}]},{},[3])(3)
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJidWlsZC9zcmMvY2FsY3VsYXRvcl93aWRnZXQuanMiLCJidWlsZC9zcmMvbWF0aF9kZW1vLmpzIiwiYnVpbGQvdGVzdC9iZGQudGVzdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDN0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLy8vPHJlZmVyZW5jZSBwYXRoPVwiLi9pbnRlcmZhY2VzLnRzXCIgLz5cbi8vLyA8cmVmZXJlbmNlIHBhdGg9XCIuLi90eXBpbmdzL2dsb2JhbHMvanF1ZXJ5L2luZGV4LmQudHNcIiAvPlxuXCJ1c2Ugc3RyaWN0XCI7XG52YXIgQ2FsY3VsYXRvcldpZGdldCA9IChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gQ2FsY3VsYXRvcldpZGdldChtYXRoKSB7XG4gICAgICAgIGlmIChtYXRoID09IG51bGwpXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJBcmd1bWVudCBudWxsIGV4Y2VwdGlvbiFcIik7XG4gICAgICAgIHRoaXMuX21hdGggPSBtYXRoO1xuICAgIH1cbiAgICBDYWxjdWxhdG9yV2lkZ2V0LnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgJChpZCkuaHRtbCh0ZW1wbGF0ZSk7XG4gICAgICAgIHRoaXMuJGJhc2UgPSAkKFwiI2Jhc2VcIik7XG4gICAgICAgIHRoaXMuJGV4cG9uZW50ID0gJChcIiNleHBvbmVudFwiKTtcbiAgICAgICAgdGhpcy4kcmVzdWx0ID0gJChcIiNyZXN1bHRcIik7XG4gICAgICAgIHRoaXMuJGJ0biA9ICQoXCIjc3VibWl0XCIpO1xuICAgICAgICB0aGlzLiRidG4ub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgX3RoaXMub25TdWJtaXQoKTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICBDYWxjdWxhdG9yV2lkZ2V0LnByb3RvdHlwZS5vblN1Ym1pdCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGJhc2UgPSBwYXJzZUludCh0aGlzLiRiYXNlLnZhbCgpKTtcbiAgICAgICAgdmFyIGV4cG9uZW50ID0gcGFyc2VJbnQodGhpcy4kZXhwb25lbnQudmFsKCkpO1xuICAgICAgICBpZiAoaXNOYU4oYmFzZSkgfHwgaXNOYU4oZXhwb25lbnQpKSB7XG4gICAgICAgICAgICBhbGVydChcIkJhc2UgYW5kIGV4cG9uZW50IG11c3QgYmUgYSBudW1iZXIhXCIpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy4kcmVzdWx0LnZhbCh0aGlzLl9tYXRoLnBvdyhiYXNlLCBleHBvbmVudCkpO1xuICAgICAgICB9XG4gICAgfTtcbiAgICByZXR1cm4gQ2FsY3VsYXRvcldpZGdldDtcbn0oKSk7XG5leHBvcnRzLkNhbGN1bGF0b3JXaWRnZXQgPSBDYWxjdWxhdG9yV2lkZ2V0O1xuLy8gbm9ybWFsbHkgd2Ugd2lsbCB1c2UgYSB0ZW1wbGF0ZSBzeXN0ZW1cbnZhciB0ZW1wbGF0ZSA9ICc8ZGl2IGNsYXNzPVwid2VsbFwiPicgK1xuICAgICc8ZGl2IGNsYXNzPVwicm93XCI+JyArXG4gICAgJzxkaXYgY2xhc3M9XCJjb2wtbWQtM1wiPicgK1xuICAgICc8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPicgK1xuICAgICc8bGFiZWw+QmFzZTwvbGFiZWw+JyArXG4gICAgJzxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJiYXNlXCIgcGxhY2Vob2xkZXI9XCIwXCI+JyArXG4gICAgJzwvZGl2PicgK1xuICAgICc8L2Rpdj4nICtcbiAgICAnPGRpdiBjbGFzcz1cImNvbC1tZC0zXCI+JyArXG4gICAgJzxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+JyArXG4gICAgJzxsYWJlbD5FeHBvbmVudDwvbGFiZWw+JyArXG4gICAgJzxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJleHBvbmVudFwiIHBsYWNlaG9sZGVyPVwiMFwiPicgK1xuICAgICc8L2Rpdj4nICtcbiAgICAnPC9kaXY+JyArXG4gICAgJzxkaXYgY2xhc3M9XCJjb2wtbWQtM1wiPicgK1xuICAgICc8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPicgK1xuICAgICc8bGFiZWw+UmVzdWx0PC9sYWJlbD4nICtcbiAgICAnPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cInJlc3VsdFwiIHBsYWNlaG9sZGVyPVwiMVwiIGRpc2FibGVkPVwiZGlzYWJsZWRcIj4nICtcbiAgICAnPC9kaXY+JyArXG4gICAgJzwvZGl2PicgK1xuICAgICc8ZGl2IGNsYXNzPVwiY29sLW1kLTNcIj4nICtcbiAgICAnPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj4nICtcbiAgICAnPGJ1dHRvbiBpZD1cInN1Ym1pdFwiIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiPlN1Ym1pdDwvYnV0dG9uPicgK1xuICAgICc8L2Rpdj4nICtcbiAgICAnPC9kaXY+JyArXG4gICAgJzwvZGl2PicgK1xuICAgICc8L2Rpdj4nO1xuIiwiLy8vPHJlZmVyZW5jZSBwYXRoPVwiLi9pbnRlcmZhY2VzLnRzXCIgLz5cblwidXNlIHN0cmljdFwiO1xudmFyIE1hdGhEZW1vID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBNYXRoRGVtbygpIHtcbiAgICAgICAgdGhpcy5QSSA9IDMuMTQxNTkyNjUzNTk7XG4gICAgfVxuICAgIC8vIHVzZWQgdG8gc2hvd2Nhc2UgaG93IHRvIHRlc3QgYSBzeW5jIGZ1bmN0aW9uXG4gICAgTWF0aERlbW8ucHJvdG90eXBlLnBvdyA9IGZ1bmN0aW9uIChiYXNlLCBleHBvbmVudCkge1xuICAgICAgICB2YXIgcmVzdWx0ID0gYmFzZTtcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBleHBvbmVudDsgaSsrKSB7XG4gICAgICAgICAgICByZXN1bHQgPSByZXN1bHQgKiBiYXNlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfTtcbiAgICAvLyB1c2VkIHRvIHNob3cgY2FzZSBob3cgdG8gdGVzdCBhc3luYyBjb2RlIHdpdGggZG9uZSgpXG4gICAgTWF0aERlbW8ucHJvdG90eXBlLnBvd0FzeW5jID0gZnVuY3Rpb24gKGJhc2UsIGV4cG9uZW50LCBjYikge1xuICAgICAgICB2YXIgcmVzdWx0ID0gdGhpcy5wb3coYmFzZSwgZXhwb25lbnQpO1xuICAgICAgICBjYihyZXN1bHQpO1xuICAgIH07XG4gICAgLy8gc2ltdWxhdGUgc2xvdyA+IDQwbXNcbiAgICBNYXRoRGVtby5wcm90b3R5cGUucG93QXN5bmNTbG93ID0gZnVuY3Rpb24gKGJhc2UsIGV4cG9uZW50LCBjYikge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciByZXN1bHQgPSBfdGhpcy5wb3coYmFzZSwgZXhwb25lbnQpO1xuICAgICAgICAgICAgY2IocmVzdWx0KTtcbiAgICAgICAgfSwgNDUpO1xuICAgIH07XG4gICAgLy8gc2ltdWxhdGUgcmVlbGx5IHNsb3cgZnVuY3Rpb24gPiAxMDBtc1xuICAgIE1hdGhEZW1vLnByb3RvdHlwZS5wb3dBc3luY1JlYWxseVNsb3cgPSBmdW5jdGlvbiAoYmFzZSwgZXhwb25lbnQsIGNiKSB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgIHZhciByZXN1bHQgPSBiYXNlIF4gZXhwb25lbnQ7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IF90aGlzLnBvdyhiYXNlLCBleHBvbmVudCk7XG4gICAgICAgICAgICBjYihyZXN1bHQpO1xuICAgICAgICB9LCAxMDEpO1xuICAgIH07XG4gICAgLy8gc2ltdWxhdGUgdG9vIHNsb3cgPiAyMDAwbXMgKGJyZWFrcyBidWlsZClcbiAgICBNYXRoRGVtby5wcm90b3R5cGUucG93QXN5bmNUb29TbG93ID0gZnVuY3Rpb24gKGJhc2UsIGV4cG9uZW50LCBjYikge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICB2YXIgcmVzdWx0ID0gYmFzZSBeIGV4cG9uZW50O1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciByZXN1bHQgPSBfdGhpcy5wb3coYmFzZSwgZXhwb25lbnQpO1xuICAgICAgICAgICAgY2IocmVzdWx0KTtcbiAgICAgICAgfSwgMjAwMSk7XG4gICAgfTtcbiAgICAvLyB1c2VkIHRvIHNob3djYXNlIGhvdyB0byBhc3NlcnQgdGhhdCBhbiBlcnJvciBpcyB0aHJvd25cbiAgICBNYXRoRGVtby5wcm90b3R5cGUuYmFkID0gZnVuY3Rpb24gKGZvbykge1xuICAgICAgICBpZiAoZm9vID09IG51bGwpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkVycm9yIVwiKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgfVxuICAgIH07XG4gICAgcmV0dXJuIE1hdGhEZW1vO1xufSgpKTtcbmV4cG9ydHMuTWF0aERlbW8gPSBNYXRoRGVtbztcbiIsIi8vLzxyZWZlcmVuY2UgcGF0aD1cIi4uL3NyYy9pbnRlcmZhY2VzLnRzXCIgLz5cbi8vLyA8cmVmZXJlbmNlIHBhdGg9XCIuLi90eXBpbmdzL2luZGV4LmQudHNcIiAvPlxuXCJ1c2Ugc3RyaWN0XCI7XG52YXIgbWF0aF9kZW1vXzEgPSByZXF1aXJlKFwiLi4vc3JjL21hdGhfZGVtb1wiKTtcbnZhciBjYWxjdWxhdG9yX3dpZGdldF8xID0gcmVxdWlyZShcIi4uL3NyYy9jYWxjdWxhdG9yX3dpZGdldFwiKTtcbi8vIEhlcmUgd2Ugd2lsbCB3cml0ZSBzb21lIHRlc3RzIGZvciB0aGUgZGVtb3MgaW4gdGhlIHNvdXJjZVxuLy8gZGlyZWN0b3J5IHVzaW5nIGEgQkREIHN0eWxlLiBCREQgc3R5bGUgYXNzZXJ0aW9ucyBhcmVcbi8vIHByb3ZpZGVkIGJ5IGV4cGVjdCgpIGFuZCBzaG91bGQoKSB3aGljaCB1c2UgYSBjaGFpbmFibGVcbi8vIGxhbmd1YWdlIHRvIGNvbnN0cnVjdCBhc3NlcnRpb25zLiBUaGUgc2hvdWxkKClcbi8vIGZ1bmN0aW9uIGhhcyBzb21lIGlzc3VlcyB3aGVuIHVzZWQgaW4gSW50ZXJuZXQgRXhwbG9yZXIsXG4vLyBzbyBpdCB3aWxsIG5vdCBiZSB1c2VkIGluIHRoaXMgZGVtby5cbnZhciBleHBlY3QgPSBjaGFpLmV4cGVjdDsgLy8gaHR0cDovL2NoYWlqcy5jb20vZ3VpZGUvc3R5bGVzLyNleHBlY3Rcbi8vIGRlc2NyaWJlKCkgaXMgdXNlZCB0byBkZWNsYXJlIGEgdGVzdCBzdWl0ZVxuZGVzY3JpYmUoJ0JERCB0ZXN0IGV4YW1wbGUgZm9yIE1hdGhEZW1vIGNsYXNzIFxcbicsIGZ1bmN0aW9uICgpIHtcbiAgICAvLyBiZWZvcmUoKSBpcyBpbnZva2VkIG9uY2UgYmVmb3JlIEFMTCB0ZXN0c1xuICAgIGJlZm9yZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiYmVmb3JlKCkgaW52b2tlZCFcIik7XG4gICAgfSk7XG4gICAgLy8gYWZ0ZXIoKSBpbnZva2VkIG9uY2UgYWZ0ZXIgQUxMIHRlc3RzXG4gICAgYWZ0ZXIoZnVuY3Rpb24gKCkge1xuICAgICAgICBjb25zb2xlLmxvZyhcImFmdGVyKCkgaW52b2tlZCFcIik7XG4gICAgfSk7XG4gICAgLy8gYmVmb3JlRWFjaCgpIGlzIGludm9rZWQgb25jZSBiZWZvcmUgRUFDSCB0ZXN0XG4gICAgYmVmb3JlRWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiYmVmb3JlRWFjaCgpIGludm9rZWQhXCIpO1xuICAgIH0pO1xuICAgIC8vIGFmdGVyRWFjaCgpIGlzIGludm9rZWQgb25jZSBiZWZvcmUgRUFDSCB0ZXN0XG4gICAgYWZ0ZXJFYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJhZnRlckVhY2goKSBpbnZva2VkIVwiKTtcbiAgICB9KTtcbiAgICAvLyBpdCgpIGlzIGEgc2luZ2xlIHRlc3QgY29udGFpbmluZyBvbmUgb3IgbW9yZSBhc3NlcnRpb25zXG4gICAgaXQoJ3Nob3VsZCByZXR1cm4gdGhlIGNvcnJlY3QgbnVtZXJpYyB2YWx1ZSBmb3IgUEkgXFxuJywgZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgbWF0aCA9IG5ldyBtYXRoX2RlbW9fMS5NYXRoRGVtbygpO1xuICAgICAgICBleHBlY3QobWF0aC5QSSkudG8uZXF1YWxzKDMuMTQxNTkyNjUzNTkpO1xuICAgICAgICBleHBlY3QobWF0aC5QSSkudG8uYmUuYSgnbnVtYmVyJyk7XG4gICAgfSk7XG4gICAgaXQoJ3Nob3VsZCByZXR1cm4gdGhlIGNvcnJlY3QgbnVtZXJpYyB2YWx1ZSBmb3IgcG93IFxcbicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIG1hdGggPSBuZXcgbWF0aF9kZW1vXzEuTWF0aERlbW8oKTtcbiAgICAgICAgdmFyIHJlc3VsdCA9IG1hdGgucG93KDIsIDMpO1xuICAgICAgICB2YXIgZXhwZWN0ZWQgPSA4O1xuICAgICAgICBleHBlY3QocmVzdWx0KS50by5iZS5hKCdudW1iZXInKTtcbiAgICAgICAgZXhwZWN0KHJlc3VsdCkudG8uZXF1YWwoZXhwZWN0ZWQpO1xuICAgIH0pO1xuICAgIC8vIHRvIHRlc3QgYXN5bmMgY29kZSB3ZSBuZWVkIHRvIGludm9rZSBkb25lKCkgd2hlbiB0aGUgZXhlY3V0aW9uIGlzIGNvbXBsZXRlZFxuICAgIGl0KCdzaG91bGQgcmV0dXJuIHRoZSBjb3JyZWN0IG51bWVyaWMgdmFsdWUgZm9yIHBvdyAoYXN5bmMpIFxcbicsIGZ1bmN0aW9uIChkb25lKSB7XG4gICAgICAgIHZhciBtYXRoID0gbmV3IG1hdGhfZGVtb18xLk1hdGhEZW1vKCk7XG4gICAgICAgIG1hdGgucG93QXN5bmMoMiwgMywgZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgICAgICAgdmFyIGV4cGVjdGVkID0gODtcbiAgICAgICAgICAgIGV4cGVjdChyZXN1bHQpLnRvLmJlLmEoJ251bWJlcicpO1xuICAgICAgICAgICAgZXhwZWN0KHJlc3VsdCkudG8uZXF1YWwoZXhwZWN0ZWQpO1xuICAgICAgICAgICAgZG9uZSgpOyAvLyBpbnZva2UgZG9uZSgpIGluc2lkZSB5b3VyIGNhbGwgYmFjayBvciBmdWxmaWxsZWQgcHJvbWlzZXNcbiAgICAgICAgfSk7XG4gICAgfSk7XG4gICAgLypcbiAgICAvLyBXaGVuIHRlc3RpbmcgYXN5bmMgY29kZSBtb2NoYSB3aWxsIGxldCB1cyBrbm93IGlmIGEgZnVuY3Rpb24gdGFrZXMgdG9vIGxvbmdcbiAgICAvLyB0byBmaW5pc2ggaXRzIGV4ZWN1dGlvbi4gVGhlcmUgYXJlIDMgbGV2ZWxzIG9mIHdhcm5pbmdcbiAgICAvLyAxLiA+ICAgNDBtcyBsb3cgd2FybmluZ1xuICAgIC8vIDIuID4gIDEwMG1zIHdhcm5pbmdcbiAgICAvLyAzLiA+IDIwMDBtcyBmYXRhbCBlcnJvciAoZXhlY3V0aW9uIG9mIHRlc3Qgd2lsbCBub3QgY29udGludWUpXG4gIFxuICAgIC8vIHRoaXMgZnVuY3Rpb24gdGFrZXMgb3ZlciAyMDAwbXMgdG8gY29tcGxldGUgYW5kXG4gICAgLy8gd2lsbCB0aGVyZW9mb3JlIHN0b3AgdGhlIHRlc3QgZXhlY3V0aW9uXG4gIFxuICAgIGl0KCd0b28gc2xvdyB3aWxsIGNhdXNlIGJ1aWxkIHRvIGZhaWxcXG4nLCAoZG9uZSkgPT4ge1xuICAgICAgdmFyIG1hdGggOiBNYXRoSW50ZXJmYWNlID0gbmV3IE1hdGhEZW1vKCk7XG4gICAgICBtYXRoLnBvd0FzeW5jVG9vU2xvdygyLCAzLCBmdW5jdGlvbihyZXN1bHQpe1xuICAgICAgICB2YXIgZXhwZWN0ZWQgPSA4O1xuICAgICAgICBleHBlY3QocmVzdWx0KS50by5iZS5hKCdudW1iZXInKTtcbiAgICAgICAgZXhwZWN0KHJlc3VsdCkudG8uZXF1YWwoZXhwZWN0ZWQpO1xuICAgICAgICBkb25lKCk7IC8vIGludm9rZSBkb25lKCkgaW5zaWRlIHlvdXIgY2FsbCBiYWNrIG9yIGZ1bGZpbGxlZCBwcm9taXNlc1xuICAgICAgfSk7XG4gICAgfSk7XG4gICAgKi9cbiAgICAvLyBob3cgdG8gdGVzdCBmb3IgZXJyb3JzXG4gICAgaXQoJ3Nob3VsZCB0aHJvdyBhbiBleGNlcHRpb24gd2hlbiBubyBwYXJhbWV0ZXJzIHBhc3NlZCBcXG4nLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBtYXRoID0gbmV3IG1hdGhfZGVtb18xLk1hdGhEZW1vKCk7XG4gICAgICAgIGV4cGVjdChtYXRoLmJhZCkudG8udGhyb3coRXJyb3IpO1xuICAgIH0pO1xufSk7XG5kZXNjcmliZSgnQkREIHRlc3QgZXhhbXBsZSBmb3IgQ2FsY3VsYXRvcldpZGdldCBjbGFzcyBcXG4nLCBmdW5jdGlvbiAoKSB7XG4gICAgYmVmb3JlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJChcImJvZHlcIikuYXBwZW5kKCc8ZGl2IGlkPVwid2lkZ2V0XCIvPicpO1xuICAgIH0pO1xuICAgIGJlZm9yZUVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAkKFwiI3dpZGdldFwiKS5lbXB0eSgpO1xuICAgIH0pO1xuICAgIC8vIHNob3djYXNlcyBob3cgdG8gc3B5IG9uIGZ1bmN0aW9ucyB0byBhc3NlcnQgdGhhdCBhIGZ1bmN0aW9uIGhhcyBiZWVuIGludm9rZWRcbiAgICBpdCgnc2hvdWxkIGludm9rZSBvblN1Ym1pdCB3aGVuICNzdWJtaXQuY2xpY2sgaXMgdHJpZ2dlcmVkIFxcbicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIG1hdGggPSBuZXcgbWF0aF9kZW1vXzEuTWF0aERlbW8oKTtcbiAgICAgICAgdmFyIGNhbGN1bGF0b3IgPSBuZXcgY2FsY3VsYXRvcl93aWRnZXRfMS5DYWxjdWxhdG9yV2lkZ2V0KG1hdGgpO1xuICAgICAgICBjYWxjdWxhdG9yLnJlbmRlcihcIiN3aWRnZXRcIik7XG4gICAgICAgIC8vIHNweSBvbiBvblN1Ym1pdFxuICAgICAgICB2YXIgb25TdWJtaXRTcHkgPSBzaW5vbi5zcHkoY2FsY3VsYXRvciwgXCJvblN1Ym1pdFwiKTtcbiAgICAgICAgLy8gaW5pdGlhbGl6ZSBpbnB1dHMgYW5kIHRyaWdnZXIgY2xpY2sgb24gI3N1Ym1pdFxuICAgICAgICAkKCcjYmFzZScpLnZhbChcIjJcIik7XG4gICAgICAgICQoJyNleHBvbmVudCcpLnZhbChcIjNcIik7XG4gICAgICAgICQoXCIjc3VibWl0XCIpLnRyaWdnZXIoXCJjbGlja1wiKTtcbiAgICAgICAgLy8gYXNzZXJ0IGNhbGN1bGF0b3Iub25TdWJtaXQgd2FzIGludm9rZWRcbiAgICAgICAgZXhwZWN0KG9uU3VibWl0U3B5LmNhbGxlZCkudG8uZXF1YWwodHJ1ZSk7XG4gICAgICAgIGV4cGVjdChvblN1Ym1pdFNweS5jYWxsQ291bnQpLnRvLmVxdWFsKDEpO1xuICAgICAgICBleHBlY3QoJChcIiNyZXN1bHRcIikudmFsKCkpLnRvLmVxdWFsKCc4Jyk7XG4gICAgfSk7XG4gICAgLy8gc2hvd2Nhc2VzIGhvdyB0byB1c2Ugc3R1YiB0byBpc29sYXRlIGEgY29tcG9uZW50IGJlaW5nXG4gICAgLy8gdGVzdGVkIChDYWxjdWxhdG9yV2lkZ2V0KSBmcm9tIGl0cyBkZXBlbmRlbmNpZXMgKE1hdGhEZW1vKVxuICAgIC8vIGFsc28gc2hvd2Nhc2VzIGhvdyB0byB0ZXN0IGFzeW5jIGNvZGVcbiAgICBpdCgnb25TdWJtaXQgc2hvdWxkIHNldCAjcmVzdWx0IHZhbHVlIHdoZW4gI3N1Ym1pdC5jbGljayBpcyB0cmlnZ2VyZWQgXFxuJywgZnVuY3Rpb24gKGRvbmUpIHtcbiAgICAgICAgdmFyIG1hdGggPSBuZXcgbWF0aF9kZW1vXzEuTWF0aERlbW8oKTtcbiAgICAgICAgLy8gcmVwbGFjZSBwb3cgbWV0aG9kIHdpdGggc3R1YlxuICAgICAgICBzaW5vbi5zdHViKG1hdGgsIFwicG93XCIsIGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICAgICAgICAvLyBhc3NlcnQgdGhhdCBDYWxjdWxhdG9yV2lkZ2V0Lm9uU3VibWl0IGlzIGludm9raW5nXG4gICAgICAgICAgICAvLyBtYXRoLnBvdyB3aXRoIHRoZSByaWd0aCBhcmd1bWVudHNcbiAgICAgICAgICAgIGV4cGVjdChhKS50by5lcXVhbCgyKTtcbiAgICAgICAgICAgIGV4cGVjdChiKS50by5lcXVhbCgzKTtcbiAgICAgICAgICAgIGRvbmUoKTtcbiAgICAgICAgfSk7XG4gICAgICAgIC8vIGluaXRpYWxpemUgaW5wdXRzIGFuZCB0cmlnZ2VyIGNsaWNrIG9uICNzdWJtaXRcbiAgICAgICAgdmFyIGNhbGN1bGF0b3IgPSBuZXcgY2FsY3VsYXRvcl93aWRnZXRfMS5DYWxjdWxhdG9yV2lkZ2V0KG1hdGgpO1xuICAgICAgICBjYWxjdWxhdG9yLnJlbmRlcihcIiN3aWRnZXRcIik7XG4gICAgICAgICQoJyNiYXNlJykudmFsKFwiMlwiKTtcbiAgICAgICAgJCgnI2V4cG9uZW50JykudmFsKFwiM1wiKTtcbiAgICAgICAgJChcIiNzdWJtaXRcIikudHJpZ2dlcihcImNsaWNrXCIpO1xuICAgIH0pO1xufSk7XG4iXX0=
