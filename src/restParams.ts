import * as books from './books';

function CheckoutBooks(customer: string, ...bookIds: number[]): string[] {
    console.log(`Checking out books for ${customer}`);

    let booksCheckedOut: string[] = [];

    bookIds.forEach(id => {
        let book = books.GetBookById(id);

        if (book != null && book.available)
            booksCheckedOut.push(book.title);
    });

    return booksCheckedOut;
}
export { CheckoutBooks };