/**
 * Created by Ivan on 4/18/16.
 */

interface ILogger {

    log(arg:any):void;
}

class Logger implements ILogger {

    log(arg:any) {
        if (typeof console.log === "function")
            console.log(arg);
        else
            alert(arg);
    }
}
