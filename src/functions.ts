/// <reference path="../typings/globals/jquery/index.d.ts" />
/// <reference path="../typings/globals/handlebars/index.d.ts" />
/// <reference path="../typings/globals/Q/index.d.ts" />

//Function Types
var greetUnnamed: (name: string) => string;

greetUnnamed = function (name: string): string {
    if (name)
        return 'Hi' + name;
};

//Declaration and Assignment
var greetUnnamed2: (name: string) => string = function (name: string): string {
    if (name)
        return 'Hi' + name;
}

//Optional Parameters
function add(foo: number, bar: number, foobar?: number): number {
    foo += bar;
    if (foobar !== undefined)
        foo += foobar;

    return foo;
}

//Default Parameters
function add2(foo: number, bar: number, foobar: number = 0) {
    return foo + bar + foobar;
}

//Rest Parameters
function restAdd(...foo: number[]): number {
    var result = 0;
    for (var i = 0; i < foo.length; i++) {
        result += foo[i];
    }
    return result;
}
/*
 * add(); // returns 0
 * add(2); // returns 2
 * add(2,2,2); // returns 6
 */

//Use an array to avoid additional iteration
function add4(foo: number[]): number {
    var result = 0;
    for (var i = 0; i < foo.length; i++) {
        result += foo[i];
    }
    return result;
}
/*
 * add([]); // returns 0
 * add([2]); // returns 2
 * add([2,2]); // returns 4
 */

//Function Overloading
//Function signatures:
function overload(name: string): string; // overloaded signature
function overload(age: number): string; // overloaded signature
function overload(single: boolean): string; // overloaded signature

//All signatures must be compatible (same return type).

//Implementation signature - always last in the list.
function overload(value: (string | number | boolean)): string {
    switch (typeof value) {
        case "string":
            return `My name is ${value}.`;
        case "number":
            return `I'm ${value} years old.`;
        case "boolean":
            return value ? "I'm single." : "I'm not single.";
        default:
            console.log("Invalid Operation!");
    }
}

//Specialized overloaded signatures - same name and number of params, but
//diff return type.
interface Document {
    createElement(tagName: "div"): HTMLDivElement; // specialized
    createElement(tagName: "span"): HTMLSpanElement; // specialized
    createElement(tagName: "canvas"): HTMLCanvasElement; // specialized
    createElement(tagName: string): HTMLElement; // non-specialized
}

//When we declare a specialized signature in an object, it must be
//assignable to at least one non-specialized signature in the same object.

//Function Scope
function foo(): void {
    if (true) {
        var bar: number = 0;
    }
    alert(bar);
}
//foo(); shows 0

//If we invoke the foo function, the alert function will be able to display the variable
//bar without errors because all the variables inside a function will be in the scope of
//the entire function body, even if they are inside another block of code (except a
//function block). At runtime, all the variable declarations are moved to the top of a
//function before the function is executed. This behavior is called hoisting.

//Using let - it allows us to set the scope of a variable to a block (if, while, for...)
//rather than a function block.

function foo2(): void {
    if (true) {
        let bar: number = 0;
        bar = 1;
    }
    //alert(bar); error!
}

//While variables defined with const follow the same scope rules as variables declared with let,
//they can't be reassigned:
function foo3(): void {
    if (true) {
        const bar: number = 0;
        //bar = 1; error!
    }
    //alert(bar); error!
}

//Classes (IIFE's)
class Counter {
    private _i: number;

    constructor() {
        this._i = 0;
    }

    get(): number {
        return this._i;
    }

    set(val: number): void {
        this._i = val;
    }

    increment(): void {
        this._i++;
    }
}

var counter = new Counter();
console.log(counter.get()); // 0
counter.set(2);
console.log(counter.get()); // 2
counter.increment();
console.log(counter.get()); // 3
//console.log(counter._i); // Error: Property '_i' is private

//Generics
class User {
    name: string;
    age: number;
}

class Order {
    id: number;
    total: number;
    items: any[];
}

function getUsers(cb: (users: User[]) => void): void {
    $.ajax({
        url: 'api/users',
        method: 'GET',
        success: function (data) {
            cb(data.items);
        },
        error: function (error) {
            cb(null);
        }
    });
}

getUsers(function (users: User[]): void {
    for (let i = 0; i < users.length; i++) {
        console.log(users[i].name);
    }
});

//Generic function
function getEntities<T>(url: string, cb: (list: T[]) => void) {
    $.ajax({
        url: 'api/users',
        method: 'GET',
        success: function (data) {
            cb(data.items);
        },
        error: function (error) {
            cb(null);
        }
    });
}

//Usage
getEntities<User>('/api/users', function (users: User[]): void {
    for (let i: number; users.length; i++) {
        console.log(users[i].name);
    }
});

getEntities<Order>('/api/orders', function (orders: Order[]): void {
    for (let i: number; orders.length; i++) {
        console.log(orders[i].total);
    }
});

function Purge<T>(inventory: Array<T>): Array<T> {    
    return inventory.splice(2, inventory.length);
}

//Tag functions
function htmlEscape(literals: any, ...placeholders: any[]) { //rest parameter
    let result = "";
    for (let i = 0; i < placeholders.length; i++) {
        result += literals[i];
        result += placeholders[i]
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }
    result += literals[literals.length - 1];
    return result;
}

var html = htmlEscape`<h1>${name}</h1>`;

// The preceding function iterates through the literals and values and ensures
// that the HTML code is escaped from the values to avoid possible code injection attacks.
// The main benefit of using a tagged function is that it allows us to create custom
// template string processors.

//Asynchronous programming
var foo4 = function () { //callback function
    console.log('foo');
}

function bar4(cb: () => void) { //higher-order function
    console.log('bar');
    cb();
}

bar4(foo4); // prints 'bar' then prints 'foo'.

/*
 * In TypeScript, functions can be passed as arguments to another function. The function passed to
 * another as an argument is known as a callback. Functions can also be returned by another function.
 * The functions that accept functions as parameters (callbacks) or return functions as an argument are
 * known as higher-order functions. Callbacks are usually anonymous functions.
 */

/*
 * Promises:
 * The core idea behind promises is that a promise represents the result of an asynchronous
 * operation. Promise must be in one of the three following states:
 *
 * Pending: The initial state of a promise
 * Fulfilled: The state of a promise representing a successful operation
 * Rejected: The state of a promise representing a failed operation
 */

//Calback hell
type cb = (json: any) => void; // alias (custom type)

class View {
    constructor(config: any) {
        this._container = config.container;
        this._templateUrl = config.templateUrl;
        this._serviceUrl = config.serviceUrl;
        this._args = config.args;
    }

    private _loadJson(url: string, args: any, cb: cb, errorCb: cb) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: args,
            success: (json) => {
                cb(json);
            },
            error: (json) => {
                errorCb(json);
            }
        });
    }

    private _loadHbs(url: string, cb: cb, errorCb: cb) {
        $.ajax({
            url: url,
            type: "GET",
            dataType: "text",
            success: (hbs) => {
                cb(hbs);
            },
            error: (e) => {
                errorCb(e);
            }
        });
    }

    private _compileHbs(hbs: string, cb: cb, errorCb: cb) {
        try {
            var template = Handlebars.compile(hbs);
            cb(template);
        }
        catch (e) {
            errorCb(e);
        }
    }

    private _jsonToHtml(template: any, json: any, cb: cb, errorCb: cb) {
        try {
            var html = template(json);
            cb(html);
        }
        catch (e) {
            errorCb(e);
        }
    }

    private _appendHtml = function (html: string, cb: cb, errorCb: cb) {
        try {
            if ($(this._container).length === 0) {
                throw new Error("Container not found!");
            }
            $(this._container).html(html);
            cb($(this._container));
        }
        catch (e) {
            errorCb(e);
        }
    }

    public render(cb: cb, errorCb: cb) {
        try {
            this._loadJson(this._serviceUrl, this._args, (json) => {
                this._loadHbs(this._templateUrl, (hbs) => {
                    this._compileHbs(hbs, (template) => {
                        this._jsonToHtml(template, json, (html) => {
                            this._appendHtml(html, cb, errorCb);
                        }, errorCb);
                    }, errorCb);
                }, errorCb);
            }, errorCb);
        }
        catch (e) {
            errorCb(e);
        }
    }

    private _args: any;
    private _container: string;
    private _templateUrl: string;
    private _serviceUrl: string;
}

class ViewAsync {

    constructor(config: any) {
        this._container = config.container;
        this._templateUrl = config.templateUrl;
        this._serviceUrl = config.serviceUrl;
        this._args = config.args;
    }

    private _loadJsonAsync(url: string, args: any) {
        return Q.Promise((resolve, reject) => {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                data: args,
                success: (json) => {
                    resolve(json);
                },
                error: (e) => {
                    reject(e);
                }
            });
        });
    }

    private _loadHbsAsync(url: string) {
        return Q.Promise(function (resolve, reject) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "text",
                success: (hbs) => {
                    resolve(hbs);
                },
                error: (e) => {
                    reject(e);
                }
            });
        });
    }

    private _compileHbsAsync(hbs: string) {
        return Q.Promise(function (resolve, reject) {
            try {
                var template: any = Handlebars.compile(hbs);
                resolve(template);
            }
            catch (e) {
                reject(e);
            }
        });
    }

    private _jsonToHtmlAsync(template: any, json: any) {
        return Q.Promise(function (resolve, reject) {
            try {
                var html = template(json);
                resolve(html);
            }
            catch (e) {
                reject(e);
            }
        });
    }

    private _appendHtmlAsync(html: string, container: string) {
        return Q.Promise((resolve, reject) => {
            try {
                var $container: any = $(container);
                if ($container.length === 0) {
                    throw new Error("Container not found!");
                }
                $container.html(html);
                resolve($container);
            }
            catch (e) {
                reject(e);
            }
        });
    }

    public renderAsync() { // Composite
        return Q.Promise((resolve, reject) => {
            try {
                // assign promise to getJson - Series
                var getJson = this._loadJsonAsync(this._serviceUrl, this._args);

                // assign promise to getTemplate - Waterfall
                var getTemplate = this._loadHbsAsync(this._templateUrl)
                    .then(this._compileHbsAsync);

                // execute promises in parallel - Concurrent
                Q.all([getJson, getTemplate]).then((results) => {
                    var json = results[0];
                    var template = results[1];

                    this._jsonToHtmlAsync(template, json)
                        .then((html: string) => {
                            return this._appendHtmlAsync(html, this._container);
                        })
                        .then(($container: any) => {
                            resolve($container);
                        });
                });
            }
            catch (error) {
                reject(error);
            }
        });
    }

    private _args: any;
    private _container: string;
    private _templateUrl: string;
    private _serviceUrl: string;
}
