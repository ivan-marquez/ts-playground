/**
 * ReferenceItem
 */
class ReferenceItem {

    constructor(public title: string, private year: number) {
        console.log('Creating new ReferenceItem...');
    }

    printItem(): void {
        console.log(`${this.title} was published in ${this.year}`);
        console.log(`Department: ${ReferenceItem.department}`);
    }

    get publisher(): string {
        return this._publisher.toUpperCase();
    }

    set publisher(newPublisher: string) {
        this._publisher = newPublisher;
    }

    private _publisher: string;
    public static department: string = 'Research';
}

export { ReferenceItem };