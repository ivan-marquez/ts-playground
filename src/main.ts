import Greeter from './greeter';
import * as books from "./books";
import * as classes from "./classes";
import { MathDemo } from "./math_demo";
import * as restParams from "./restParams";
import { ReferenceItem } from "./PS_Classes";
import * as arrowfunction from "./arrowFunctions";
import { CalculatorWidget } from "./calculator_widget";
import { IdGenerator, PublicationMessage } from "./functionTypes";
import { IBook, IStringGenerator, IDamageLogger, CalculatorWidgetInterface } from "./interfaces";

/** Arrow functions **/
arrowfunction.TestArrowFunctions();

/** Function types **/
let publishFunc: (someYear: number) => string;
// do not confuse with arrow functions. this is a function type.

// because it has the same signature:
publishFunc = PublicationMessage;

let myId: string = IdGenerator("Ivan", 20);
console.log(myId);

let message = publishFunc(2016);

/** Rest Params **/
let myBooks: string[] = restParams.CheckoutBooks("Thorne", 1, 3, 4);
myBooks.forEach(title => console.log(title));

/** Class Expressions **/
let myDevExpression = new classes.devExpression("Ivan", "Marquez", new classes.Email("ivan@test.com"));

/** Interfaces **/
let book = books.GetAllBooks()[0]; // .First();
book.markDamaged("missing back cover.");

let logDamaged: IDamageLogger;
logDamaged = (damage: string) => console.log(`Damage Reported: ${damage}`);
logDamaged("coffee stains");

/** Classes **/
let refItem: ReferenceItem = new ReferenceItem("Updated Facts and Figures", 2012);
refItem.printItem();
refItem.publisher = "Random Data Publishing";
console.log(refItem.publisher);

/** Github Example **/
let greeter = new Greeter("world!");
let msg = greeter.greet();
$("body").html(`<h1>${msg}</h1>`);

/** TDD Demo **/

var math = new MathDemo();

var calculator : CalculatorWidgetInterface = new CalculatorWidget(math);
(<any>window).calculator = calculator;

calculator.render('#widget');
