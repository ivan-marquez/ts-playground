import * as Q from 'q';
import * as $ from 'jquery';

/*
 * The Email class allows us to use emails without having to worry about e-mail validation
 * because the class will deal with it for us. We can make this clearer by using access
 * modifiers (public or private) to flag as private all the class attributes and methods
 * that we want to abstract from the use of the Email class:
 */

class Email { //class responsible for emai validation.

    constructor(email: string) {
        if (this.validateEmail(email))
            this._email = email;
        else
            throw new Error("Invalid email!");
    }

    private validateEmail(email: string) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    get(): string {
        return this._email;
    }

    // get Dev(){
    //     return this._email;
    // }

    private _email: string;
}

/*
 * Single Responsibility Principle:
 * Making sure that a class has a single responsibility makes it easier to see what it does
 * and how we can extend/improve it. We can further improve our Person and Email classes by
 * increasing the level of abstraction of our classes. For example, when we use the Email class,
 * we don't really need to be aware of the existence of the validateEmail method; so this method
 * could be invisible from outside the Email class. As a result, the Email class would be much
 * simpler to understand.
 */

abstract class Person {

    constructor(name: string, surname: string, email: Email) {
        this.email = email;
        this.name = name;
        this.surname = surname;
    }

    greet() {
        console.log("Hi!");
    }

    abstract Dev(): void;

    public name: string;
    public surname: string;
    public email: Email;
}

// Inheritance

class Teacher extends Person {

    constructor(name: string, surname: string, email: Email, subjects: string[]) {
        super(name, surname, email); //parent class constructor
        this._subjects = subjects
    }

    teach() {
        console.log('Welcome to class!');
    }

    greet() {
        super.greet();
        console.log('I teach' + this._subjects);
    }

    get(): string[] {
        return this._subjects;
    }

    Dev(): void {

    }

    private _subjects: string[];
}

class SchoolPrincipal extends Teacher {

    manageTeachers() {
        console.log('We need to help students to get better results!');
    }
}

var principal = new SchoolPrincipal('remo', 'jansen', new Email('remo@dev.com'),
    ['math', 'science']);
principal.greet();
principal.teach();
principal.manageTeachers();

//Mixins
/*
 * We need to copy the following function somewhere in our code to be able to apply mixins.
 * This function is a well-known pattern and can be found in many books and online
 * references, including the official TypeScript handbook.
 */
function applyMixins(derivedCtor: any, baseCtors: any[]) {

    baseCtors.forEach(baseCtor => {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
            if (name !== 'constructor') {
                derivedCtor.prototype[name] = baseCtor.prototype[name];
            }
        });
    });
}

class Animal {

    eat(): string {
        return "Delicious!";
    }
}

class Mammal extends Animal {

    breathe(): string {
        return "I'm alive!";
    }

    move(): string {

        return "I can move like a mammal!";
    }
}

class WingedAnimal extends Animal {

    fly(): string {
        return "I can fly!";
    }

    move(): string {
        return "I can move like a bird!";
    }
}

class Bat implements Mammal, WingedAnimal {
    eat: () => string;
    breathe: () => string;
    fly: () => string;
    move: () => string;
}

/*
 * We have used the reserved keyword implements (as opposed to extends) to indicate that
 * Bat will implement the functionality declared in both the Mammal and WingedAnimal classes.
 * We have also added the signature of each of the methods that the Bat class will implement.
 */

applyMixins(Bat, [WingedAnimal, Mammal]);

/*
 * Mixins have some limitations. The first limitation is that we can only inherit the properties
 * and methods FROM ONE LEVEL IN THE INHERITANCE TREE. The second limitation is that, if two or
 * more of the parent classes contain a method with the same name, THE METHOD THAT IS GOING TO BE
 * INHERITED WILL BE TAKEN FROM THE LAST CLASS PASSED IN THE BASECTORS ARRAY TO THE APPLYMIXINS
 * FUNCTION.
 */

var bat = new Bat();
//bat.eat(); //Error: not avaliable.
bat.breathe();
bat.fly();
bat.move(); //I can move like a mammal!

//Generic Classes - Generic Constraints
interface IValidatable {
    isValid(): boolean;
}

class DevUser implements IValidatable {

    public isValid(): boolean {
        //User validation logic
        return true;
    }

    public name: string;
    public password: string;
}

class Talk implements IValidatable {

    public isValid(): boolean {
        //Talk validation logic
        return true;
    }

    public title: string;
    public description: string;
    public language: string;
    public url: string;
    public year: string;
}

/*
 * Open / Close Principle:
 * If we hide the validation logic through the IValidatable interface (open for extension),
 * no additional changes will be required for the Repository class (close for modification).
 */

class Repository<T extends IValidatable> {

    constructor(url: string) {
        this._url = url;
    }

    public getAsync() {
        return Q.Promise((resolve: (entities: T[]) => void,
            reject: any): void => {
            $.ajax({
                url: this._url,
                type: "GET",
                dataType: "json",
                success: (data) => {
                    let list: any;
                    let items = <T[]>data.items;
                    for (let i = 0; i < items.length; i++) {
                        if (items[i].isValid()) {
                            list.push(items[i]);
                        }
                    }
                    resolve(list);
                },
                error: (e) => {
                    reject(e);
                }
            });
        });
    }

    private _url: string;
}

/*
 * Even though we have used an interface, we used the extends keyword and not the
 * implements keyword to declare the constraint in the preceding example. THERE IS
 * NO SPECIAL REASON FOR THAT. THIS IS JUST THE WAY THE TYPESCRIPT CONSTRAINT
 * SYNTAX WORKS.
 *
 * NOTE:
 * We cannot specify multiple types when declaring a generic type constraint. However, we can
 * work around this issue by transforming the required interfaces in super-interfaces and having
 * a child interface that extends this super-interfaces.
 */

var userRepository = new Repository<DevUser>("/api/users");

userRepository.getAsync().then((users: DevUser[]): void => {
    console.log('userRepository => ', users);
});

var talkRepository = new Repository<Talk>("/api/talk");

talkRepository.getAsync().then((talks: Talk[]) => {
    console.log('talkRepository => ', talks);
});

// new operator in generic type constraints
function factory<T>(): T {

    var type: {
        new (): T;
    };

    return new type;
}

// var user: DevUser = factory<DevUser>();




/*
 * Liskov Substitution Principle:
 * SUBTYPES MUST BE SUBSTITUTABLE FOR THEIR BASE TYPES.
 */

interface IPersistable {
    save(entity: any): number;
}

class CookiePersitanceService implements IPersistable { //Implementing class

    save(entity: any): number {
        var id = Math.floor((Math.random() * 100) + 1);
        // Cookie persistance logic...
        return id;
    }
}

class LocalStoragePersitanceService implements IPersistable { //Implementing class

    save(entity: any): number {
        var id = Math.floor((Math.random() * 100) + 1);
        // Local storage persistance logic...
        return id;
    }
}

class FavouritesController { //Client class

    constructor(persistanceService: IPersistable) {
        this._persistanceService = persistanceService;
    }

    public saveAsFavorite(articleId: number) {
        return this._persistanceService.save(articleId);
    }

    /*
     * Dependency Inversion Principle
     * DI principle states, DEPEND UPON ABSTRACTIONS. DO NOT DEPEND UPON CONCRETIONS.
     * _persistanceService could be any implementation of IPersistable.
     */

    private _persistanceService: IPersistable;
}

/*
 * The LSP allows us to replace a dependency with another implementation as long as
 * both implementations are based in the same base type; so, if we decide to stop using
 * cookies as storage and use the HTML5 local storage API instead, we can do it freely
 * without having to add any changes to the FavouritesController class.
 */

//var favController = new FavouritesController(new CookiePersitanceService());
var favController = new FavouritesController(new LocalStoragePersitanceService());

/*
 * NOTE:
 * Interfaces are used to declare how two or more software components cooperate and exchange
 * information with each other. This declaration is known as application programming interface (API).
 */

/*
 * The interface segregation principle:
 * ISP STATES THAT NO CLIENT SHOULD BE FORCED TO DEPEND ON METHODS IT DOES NOT USE. To adhere to the ISP,
 * we need to keep in mind that when we declare the API of our application's components, the declaration
 * of many client-specific interfaces is better than the declaration of one general-purpose interface.
 */

interface VehicleInterface {
    getSpeed(): number;
    isTaxPayed(): boolean;
    isLightsOn(): boolean;
    isLightsOff(): boolean;
    startEngine(): void;
    acelerate(): number;
    stopEngine(): void;
    startRadio(): void;
    stopRadio(): void;

    playCd: void;
    getVehicleType: string;
}

/*
 * Split the VehicleInterface interface into many client-specific interfaces so that our classes
 * can adhere to the ISP by depending only on the needed functionality.
 */

interface IVehicle {
    getSpeed(): number;
    isTaxPayed(): boolean;
    isLightsOn(): boolean;

    getVehicleType: string;
}

interface ILight {
    isLightsOn(): boolean;
    isLightsOff(): boolean;
}

interface IRadio {
    startRadio(): void;
    stopRadio(): void;

    playCd: void;
}

interface IEngine {
    startEngine(): void;
    acelerate(): number;
    stopEngine(): void;
}

// Class Expressions
let devExpression = class extends Person {
    Dev(): void {
        console.log('implementing abstract method.');
    }
}

export { devExpression, Email };
