/*
 * Decorators:
 * A class decorator is used to modify the constructor of class in some way. If the class
 * decorator returns undefined, the original constructor remains the same. If the decorator
 * returns, the return value will be used to override the original class constructor.
 * */

///<reference path="../node_modules/reflect-metadata/reflect-metadata.d.ts" />

import 'reflect-metadata';

@log
class Entity {

    constructor(name:string, surname:string) {

        this.name = name;
        this.surname = surname;
    }

    @log
    public saySomething(@log something:string):string {

        return `${this.name} ${this.surname} says ${something}`;
    }

    @log
    public name:string;
    public surname:string;
}

/*
 * Class Decorators:
 * Decorators are used to add some extra logic or metadata to the decorated element. When we try to
 * extend the functionality of a function (methods or constructors), we need to wrap the original
 * function with a new function that contains the additional logic and invokes the original function.
 * */

function logClass(target:any) {

    // save a reference to the original constructor
    var original = target;

    // a utility function to generate instances of a class
    function construct(constructor:any, args:any) {
        var c:any = ()=> {
            return constructor.apply(this, args);
        }
        c.prototype = constructor.prototype;
        return new c();
    }

    // the new constructor behaviour
    var f:any = function (...args:any[]) {
        console.log("New: " + original.name);
        return construct(original, args);
    }

    // copy prototype so instanceof operator still works
    f.prototype = original.prototype;

    // return new constructor (will override original)
    return f;

}

/*
 * Method Decorators:
 * A method decorator function is a function that accepts three arguments: The object that owns the property (target),
 * the key for the property (a string or a symbol), and optionally the property descriptor of the property.
 *
 * The method decorator is really similar to the class decorator but it is used to override a method, as
 * opposed to using it to override the constructor of a class. If the method decorator returns a value different
 * from undefined, the returned value will be used to override the property descriptor of the method.
 *
 * The method decorator is invoked using the following arguments:
 *
 * The prototype of the class that contains the method being decorated is Person.prototype
 * The name of the method being decorated is saySomething
 * The property descriptor of the method being decorated is Object.getOwnPropertyDescriptor(Person.prototype,
 * saySomething)
 * */

function logMethod(target:any, key:string, descriptor:any) {

    // save a reference to the original method
    var originalMethod = descriptor.value;

    // editing the descriptor/value parameter (decorated method)
    descriptor.value = function (...args:any[]) {

        // convert method arguments to string
        var arg = args.map(a => JSON.stringify(a)).join();

        // invoke method and get its return value
        var result = originalMethod.apply(this, args);

        // convert result to string
        var r = JSON.stringify(result);

        // display in console the function call details
        console.log(`Call: ${key}(${arg}) => ${r}`);

        // return the result of invoking the method
        return result;
    }

    // return edited descriptor
    return descriptor;
}

/*
 * Property Decorators:
 * A property decorator is really similar to a method decorator. The main differences are that a property decorator
 * doesn't return a value and that the third parameter (the property descriptor) is not passed to the property
 * decorator.
 *
 * NOTE:
 * Because the property decorator doesn't return a value, we can't override the property being decorated but we can
 * replace it.
 * */

function logProperty(target:any, key:string) {

    // property value
    var _val = this[key];

    // property getter
    var getter = function () {
        console.log(`Get: ${key} => ${_val}`);
        return _val;
    };

    // property setter
    var setter = function (newVal:any) {
        console.log(`Set: ${key} => ${newVal}`);
        _val = newVal;
    };

    // Delete property. The delete operator throws in strict mode if the property is an own
    // non-configurable property and returns false in non-strict mode.
    if (delete this[key]) {
        Object.defineProperty(target, key, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    }
}

/*
 * Parameter Decorators:
 * A parameter decorator function is a function that accepts three arguments: The object that owns the method that
 * contains the decorated parameter (target), the property key of the property (or undefined for a parameter of the
 * constructor), and the ordinal index of the parameter. The return value of this decorator is ignored.
 *
 * To allow more than one parameter to be decorated, we check whether the new field is an array. If the new field is not
 * an array, we create and initialize the new field to be a new array containing the index of the parameter being
 * decorated. If the new field is an array, the index of the parameter being decorated is added to the array.
 *
 * NOTE:
 * A parameter decorator is not really useful on its own; it needs to be combined with a method decorator, so the
 * parameter decorator adds the metadata and the method decorator reads it.
 * */

function addMetadata(target:any, key:string, index:number) {

    let metadataKey = `_log_${key}_parameters`;

    if (Array.isArray(target[metadataKey]))
        target[metadataKey].push(index);
    else
        target[metadataKey] = [index];
}

/*
 * The following method decorator works like the method decorator that we implemented previously in this chapter, but it
 * will read the metadata added by the parameter decorator and instead of displaying all the arguments passed to the
 * method in the console when it is invoked, it will only log the ones that have been decorated.
 *
 * The readMetadata decorator will display the value of the parameters that were added to the metadata (the class
 * property named _log_saySomething_parameters) in the console by the addMetadata decorator:
 * */

function readMetadata(target:any, key:string, descriptor:any) {

    let originalMethod = descriptor.value;
    descriptor.value = function (...args:any[]) {

        let metadataKey = `_log_${key}_parameters`;
        let indices = target[metadataKey];

        if (Array.isArray(indices)) {
            for (let i = 0; i < args.length; i++) {

                if (indices.indexOf(i) !== -1) {
                    let arg = args[i];
                    let argStr = JSON.stringify(arg) || arg.toString();

                    console.log(`${key} arg[${i}]: ${argStr}`);
                }
            }

            let result = originalMethod.apply(this, args);

            return result;
        }
    }

    return descriptor;
}

/*
 * Decorator Factory:
 * A decorator factory is a function that can accept any number of arguments, and must return one of the above types of
 * decorator function (class, method, property, and argument decorators).
 *
 * You learned to implement class, property, method, and parameter decorators. In the majority of cases, we will consume
 * decorators, not implement them.
 *
 * For example, in Angular 2.0, we will use an @view decorator to declare that a class will behave as a View, but we
 * will not implement the @view decorator ourselves.
 *
 * A decorator factory is a function that is able to identify what kind of decorator is required and return it.
 * */

function log(...args:any[]) {

    switch (args.length) {
        case 1:
            return logClass.apply(this, args);
        case 2:
            // break instead of return as property decorators don't have a return
            logProperty.apply(this, args);
            break;
        case 3:
            if (typeof args[2] === "number")
                addMetadata.apply(this, args);

            return logMethod.apply(this, args);
        default:
            throw new Error("Decorators are not valid here!");
    }
}

/*
 * Decorators with Arguments:
 * In order to be able to pass some parameters to a decorator, we need to wrap the decorator with a function. The wrapper
 * function takes the parameters of our choice and returns a decorator.
 *
 * NOTE:
 * This can be applied to all decorators.
 * */

function logClassConfig(option:string) {

    return function (target:any) {
        // class decorator logic goes here
        // we have access to the decorator parameters
        console.log(target, option);
    }
}

/*
 * Reflection Metadata API:
 * The TypeScript documentation defines three reserved metadata keys:
 *
 * Type metadata uses the metadata key "design:type".
 * Parameter type metadata uses the metadata key "design:paramTypes".
 * Return type metadata uses the metadata key "design:returnType".
 *
 * NOTE:
 * The types are serialized and follow some rules. We can see that functions are serialized as Function, objects
 * literals ({test : string}) and interfaces are serialized as Object, and so on.
 * */

class Demo {

    @logParamTypes
    public doSomething(param1:string, param4:{ test:string }, param6:Function, param7:(a:number) => void):number {

        return 1;
    }

    @logReturnType
    public doSomething2():string {
        return "test";
    }

    @logType
    public attr1:string;
}

function logType(target:any, key:string) {

    var t = Reflect.getMetadata('design:type', target, key);

    console.log(`${key} type: ${t.name}`);
    //attr1 type: String
}

function logParamTypes(target:any, key:string) {

    var types = Reflect.getMetadata('design:paramtypes', target, key);
    var s = types.map(a => a.name).join();

    console.log(`${key} param types: ${s}`);
    //doSomething param types: String, Object, Function, Func
}

function logReturnType(target, key) {

    var returnType = Reflect.getMetadata('design:returntype', target, key);

    console.log(`${key} return type: ${returnType.name}`);
    //doSomething2 return type: String
}

export { Entity };