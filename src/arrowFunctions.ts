import * as books from './books';

/*
 * Arrow functions
 * An arrow function expression has a shorter syntax compared to function expressions and lexically binds
 * the value of the this operator.
 */
class Person2 {    
    constructor(name: string) {
        this.name = name;
    }
    greet() {
        alert(`Hi! My name is ${this.name}`); // this operator points to the object that encloses the greet method.
    }
    /*
     * As soon as we define an anonymous function (the callback), the this keyword changes its value and starts
     * pointing to the anonymous function.
     */
    greetDelay(time: number) {
        setTimeout(function () {
            alert(`Hi! My name is ${this.name}`);
        }, time);
    }
    /*
     * arrow functions allows us to add a function without altering the value of this operator.
     * By using an arrow function, we can ensure that the this operator still points to the Person
     * instance and not to the setTimeout callback.
     */
    greetDelay2(time: number) {
        setTimeout(() => {
            alert(`Hi! My name is ${this.name}`);
        }, time);
    }
    
    name: string;
}

// var remo = new Person2("Remo");
// remo.greet(); // "Hi! My name is Remo
// remo.greetDelay(1000); // "Hi! My name is
// remo.greetDelay2(1000); // "Hi! My name is Remo

function TestArrowFunctions() {    
    const allBooks = books.GetAllBooks();
    books.LogFirstAvailable(allBooks);
    
    let arr = allBooks.filter(function (book) {
        return book.author == 'Herman Melville';
    });

    // Same function written in arrow syntax (with type annotations)
    arr = allBooks.filter((book: any): boolean => book.author == 'Herman Melville');

    // Same function written in arrow syntax (short way)
    arr = allBooks.filter(book => book.author == 'Herman Melville');

    // no params: parenthesis required.
    allBooks.forEach(() => console.log('done reading!'));

    // 1 param: parenthesis are optional.
    allBooks.forEach(title => console.log(title));

    // multiple params: parenthesis required.
    allBooks.forEach((book, idx, arr) => console.log(`${idx} - ${book.title}`));

    /*
    * Note:
    * In arrow functions, everything on the left is a parameter, and everything on the right is
    * the function body. if the function does not accept a parameter, or if there's more than
    * one parameter, you must add a set of parenthesis.
    * 
    * Arrow functions lexically binds the value of the this keyword. In other
    * words, the keyword is captured at function creation, not invocation.
    * 
    * Side note:
    * the 'this' keyword is set at the time a function is executed.
    */
}

export { TestArrowFunctions };