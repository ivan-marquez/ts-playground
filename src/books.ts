import { Category } from './enums';
import { IBook } from './interfaces';

function GetAllBooks(): IBook[] {
    let books = [{
        id: 1,
        title: 'Ulisses',
        author: 'Jams Joyce',
        available: true,
        category: Category.Physics,
        markDamaged: (reason : string) => console.log(`Damaged: ${reason}`)
    }, {
        id: 2,
        title: 'A farewell to Arms',
        author: 'Ernest Hemingway',
        available: false,
        category: Category.Research
        }, {
        id: 3,
        title: 'I Know Why the Caged bird Sings',
        author: 'Maya Angalou',
        available: true,
        category: Category.Science
    }];

    return books;
}

function LogFirstAvailable(books: any): void {
    let numberOfBooks: number = books.length;
    let firstAvailable: string;

    for (let currentBook of books) {
        if (currentBook.available) {
            firstAvailable = currentBook.title;
            break;
        }
    }

    console.log(`Total Books: ${numberOfBooks}`);
    console.log(`First Available: ${firstAvailable}`);
}

function GetBookById(id: number): IBook {

    const allBooks = GetAllBooks();

    return allBooks.filter(book => book.id === id)[0]; //.First() in c#
}

function PrintBook(book: IBook): void {
    console.log(`${book.title} by ${book.author}`);
}

export { GetAllBooks, LogFirstAvailable, GetBookById, PrintBook };