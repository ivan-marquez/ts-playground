module customException {
    export declare class Error {
        public name: string;
        public message: string;
        public stack: string;
        constructor(message?: string);
    }

    export class Exception extends Error {

        constructor(public message: string) {
            super(message);
            this.name = 'Exception';
            this.message = message;
            this.stack = (<any>new Error()).stack;
        }

        toString(): string {
            return this.name + ': ' + this.message;
        }
    }
}

/*
 * In the preceding code snippet, we have declared a class named Error. This class is available at
 * runtime but is not declared by TypeScript, so we will have to do it ourselves. Then, we have
 * created an Exception class, which inherits from the Error class.
 */
