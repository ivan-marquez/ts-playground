import { Category } from './enums';

interface IBook {
    // markDamaged?: (reason: string) => void;    
    markDamaged?: IDamageLogger;
    id: number;
    title: string;
    author: string;    
    available: boolean;
    category: Category;
}

interface IStringGenerator {
    (chars: string, nums: number): string;
}

interface IDamageLogger {
    (reason: string): void;
}

interface MathInterface {
    PI: number;
    pow(base: number, exponent: number): any;
    powAsync(base: number, exponent: number, cb: (result: number) => void): any;
    powAsyncSlow(base: number, exponent: number, cb: (result: number) => void): any;
    powAsyncReallySlow(base: number, exponent: number, cb: (result: number) => void): any;
    powAsyncTooSlow(base: number, exponent: number, cb: (result: number) => void): any;
    bad(foo: any): void;
}

interface CalculatorWidgetInterface {
    render(id:string):any;
    onSubmit(): void;
}

interface GreeterInterface {
    greeting: string;
    greet: () => string;
}

export { IBook, IStringGenerator, IDamageLogger, MathInterface, CalculatorWidgetInterface, GreeterInterface };