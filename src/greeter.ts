import { GreeterInterface } from './interfaces'

class Greeter implements GreeterInterface {
    
    constructor(message: string) {
        this.greeting = message;
    }
    
    public greet() {
        return `Hello, ${this.greeting}`;
    }
    
    public greeting: string;
}

export default Greeter;