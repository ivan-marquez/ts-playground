import * as books from './books';
import { IStringGenerator } from './interfaces';

function PublicationMessage(year: number): string {
    return `Date published: ${year}`;
}

function CreateCustomerId(name: string, id: number): string {
    return `${name} ${id}`;
}

function GetBookById(id: number): Object {
    return books.GetAllBooks().filter(book => book.id === id);
}

//let IdGenerator: (chars: string, nums: number) => string;
let IdGenerator: IStringGenerator = (name: string, id: number) => { return `${id} ${name}` };

export { PublicationMessage, CreateCustomerId, GetBookById, IdGenerator };